from django.db import models
from datetime import datetime
from django.core.exceptions import ValidationError
from studentunion_register.models import Member

# Create your models here.
class Event(models.Model):
	name = models.CharField(max_length = 100)
	location = models.CharField(max_length = 100)
	description = models.CharField(max_length = 300)
	category = models.CharField(max_length = 50)
	date = models.DateField(default=datetime.now)
	start_time = models.TimeField(default='12:00')
	end_time = models.TimeField(default='12:00')
	entrants = models.ManyToManyField(Member, blank=True)

	def clean(self):
		if self.start_time > self.end_time:
			raise ValidationError("Start time cannot be later than end time.")