from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Event
from studentunion_register.models import Member

# Create your views here.
def index(request):
	events_list = Event.objects.all().values()

	args = {'events_list': events_list}
	return render(request, 'events.html', args)

def detail(request, id):
	event = Event.objects.get(pk = id)
	args = {'event' : event}
	return render(request, 'detail.html', args)
	
@login_required
def enter(request, id):
	# print("klik")
	event = Event.objects.get(pk = id)
	user = request.user
	if not event.entrants.filter(pk = user.pk).exists():
		# print("ga ada")
		event.entrants.add(user)
		messages.info(request, 'Event entered')
	messages.info(request, 'Already entered event')
	return HttpResponseRedirect('/events/detail/' + id)
	
	