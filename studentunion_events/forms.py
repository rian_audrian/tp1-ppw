from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from .models import Entrant
import re

class EntrantForm(forms.ModelForm):
	class Meta:
		model = Entrant
		fields = '__all__'
		fields_required = '__all__'
		widgets = {
			'user_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}),
			'password': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}),
			'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Example@email.com'})
		}

	confirm_password = forms.CharField(widget = forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirm password', 'label': 'Confirm password'}))

	def clean(self):
		cleaned_data = super(EntrantForm, self).clean()
		password = cleaned_data.get('password')
		confirm = cleaned_data.get('confirm_password')

		if password != confirm:
			raise ValidationError('Passwords do not match', code='passwords_do_not_match')
		if len(password) < 8:
			raise ValidationError('Password must be at least 8 characters long', code='password_too_short')
		if not password.isalnum():
			raise ValidationError('Password must contain letters and numbers', code='password_not_alphanumeric')

		return self.cleaned_data