from django.test import TestCase, Client
from django.urls import resolve
from .views import about
# Create your tests here.

class StudentUnionAboutTest(TestCase):
    def test_about_page_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_uses_correct_template(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

class StudentUnionAboutFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StudentUnionAboutFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StudentUnionAboutFunctionalTest, self).tearDown()
    