from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import UserCreationForm
from .models import Member

class RegisterForm(UserCreationForm):
    class Meta:
        model = Member
        fields = ('name', 'username', 'email', 'dob', 'password1', 'address')
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your Name'}),
            'username' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username', 'label': 'Username'}),
            'email' : forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Example@email.com', 'label': 'Email'}),
            'dob' : forms.DateInput(attrs={'class': 'form-control', 'placeholder': 'Date of Birth', 'label': 'DOB'}),
            'address' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Address', 'label': 'Address'}),
        }
	
    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data['password1']

        if (len(password1) < 8):
            raise ValidationError("Password must be at least 8 characters long", code="pwd_too_short")
        if not password1.isalnum():
            raise ValidationError("Password must be alphanumeric", code="pwd_not_alnum")

        print(cleaned_data) #DEBUG    
        return self.cleaned_data
        
        