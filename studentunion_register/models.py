from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone

class Member(AbstractUser):
    name = models.CharField(max_length = 40)
    username = models.CharField(max_length = 40, unique = True)
    email = models.EmailField(unique = True)
    dob = models.DateField(max_length = 30, default = timezone.now)
    address = models.CharField(max_length = 100)