from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.contrib import messages
from .models import Member
from .forms import RegisterForm

def register(request):
	if request.method == "POST":
		form = RegisterForm(request.POST)
		# print(form.is_valid())
		# print(form.errors)
		if (form.is_valid()):
			form.save()
			# messages.success(request, "User registered!")
			messages.info(request, 'Registration successful!')
			return HttpResponseRedirect('/')
	form = RegisterForm()
	args = {'form' : form}
	return render(request, 'register.html', args)

