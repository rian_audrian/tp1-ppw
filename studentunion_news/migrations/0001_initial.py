# Generated by Django 2.1.1 on 2018-10-18 03:54

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='aNews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tittle', models.CharField(max_length=100)),
                ('time', models.DateField(blank=True, default=datetime.datetime.now)),
                ('content', models.CharField(max_length=300)),
            ],
        ),
    ]
