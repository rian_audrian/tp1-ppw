from django.test import TestCase, Client
from django.urls import resolve
from .views import news
from .models import aNews

# Create your tests here.
class StudentUnionNewsTest(TestCase):
    def test_news_page_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)
    
    def test_news_uses_correct_template(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news)

    def test_news_adds_anews(self):
        test = aNews.objects.create(
            tittle ="Mahasiswa KBI lulus 3,5 tahun",
            date = "2018-10-17",
            content = "Seorang mahasiswa di KBI bisa lulus 3,5 tahun. Ia berjuang keras untuk dapat lulus dengan cepat"
        )
        self.assertEqual(aNews.objects.all().count(), 1)

    def test_news_delete_anews(self):
        aNews.objects.all().delete()
        self.assertEqual(aNews.objects.all().count(), 0)